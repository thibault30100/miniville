-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP DATABASE IF EXISTS `brief1-bdd`;
CREATE DATABASE `brief1-bdd` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;
USE `brief1-bdd`;

DROP TABLE IF EXISTS `habitants`;
CREATE TABLE `habitants` (
  `ppl_id` int(11) NOT NULL AUTO_INCREMENT,
  `ppl_nom` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `ppl_prenom` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `ppl_naissance` date DEFAULT NULL,
  `ppl_ville` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ppl_emoji` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ppl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `habitants` (`ppl_id`, `ppl_nom`, `ppl_prenom`, `ppl_naissance`, `ppl_ville`, `ppl_emoji`) VALUES
(1, 'Martin', 'Patrice',  '1980-12-12', 'Cendras',  '🦊'),
(2, 'Petit',  'Camélia',  '1973-03-01', 'Saint-Paul-la-Coste',  '🌻'),
(3, 'Richard',  'Jean', '1944-03-03', 'Soustelle',  '🍄'),
(4, 'Durand', 'Rener',  '1989-10-04', 'Saint-Paul-la-Coste',  '🦊'),
(5, 'Dubois', 'Michel', '1976-03-05', 'Cendras',  '🐶'),
(6, 'Girard', 'André',  '1981-08-26', 'Corbès', '🌻'),
(7, 'Lefebre',  'Louis',  '1981-05-23', 'La Grand-Combe', '🐿'),
(8, 'Morel',  'Alain',  '1974-05-20', 'Saint-Paul-la-Coste',  '🌼'),
(9, 'Mercier',  'Marcel', '2000-11-01', 'Saint-Paul-la-Coste',  '🐼'),
(10,  'Boyer',  'Daniel', '1965-12-03', 'La Grand-Combe', '🤓'),
(11,  'Faure',  'Pierre', '1974-07-05', 'Corbès', '🌹'),
(12,  'Guerin', 'Claude', '1991-01-06', 'Anduze', '🐅'),
(13,  'Perrin', 'Marie',  '1985-01-31', 'Cendras',  '🍉'),
(14,  'Masson', 'Françoise',  '1976-09-30', 'Vézénobres', '🍔'),
(15,  'Garcia', 'Nicole', '1954-03-23', 'Montpellier',  '🏀'),
(16,  'Dumont', 'Marguerite', '2010-01-01', 'Saint-Paul-la-Coste',  '🐼'),
(17,  'Roche',  'Denise', '1943-01-23', 'Paris',  '🍷'),
(18,  'Leroux', 'Gérard', '1986-08-01', 'Saint-Paul-la-Coste',  '🦏'),
(19,  'Barbier',  'Paul', '1980-03-24', 'Anduze', '🍔'),
(20,  'Joly', 'Christiane', '1990-03-19', 'Cendras',  '🌹'),
(21,  'Aubert', 'Sophie', '1999-04-28', 'Saint-Paul-la-Coste',  '🍺');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_login` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `user_password` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `user_role` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `users` (`user_id`, `user_login`, `user_password`, `user_role`) VALUES
(1, 'guillaume@caramail.fr',  '$2y$10$bOxBMAOgriIx5XzD.2qoEeQIOWSbfHki4RZ7gc48LZh4YNt0T/8Ty', 'admin'),
(2, 'sarah@connor.fr',  '$2y$10$w3B32R.sU64TRuPy7u48iurvj/i8QciR4w443GGEgC8fFlWQ.3bEu', 'guest');

-- 2020-06-29 03:48:41